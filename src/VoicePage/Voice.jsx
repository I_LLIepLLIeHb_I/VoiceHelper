import React, { useState } from 'react';
import { useSpeechRecognition } from 'react-speech-kit';
import { useSpeechSynthesis } from 'react-speech-kit'; 
 
function Voice () {
  const [value, setValue] = useState('');
  const { listen, listening, stop } = useSpeechRecognition({
    onResult: (result) => {
      setValue(result);
    },
  });
   const { speak } = useSpeechSynthesis({
    onResult: (result) => {
      setValue(result);
    },
  });
  return (
    <div>
      <textarea
        value={value}
        onChange={(event) => setValue(event.target.value)}
      />
      <button onMouseDown={listen} onMouseUp={stop}>
        🎤
      </button>
      {listening && <div>Go ahead I'm listening</div>}
      <button onClick={() => speak({ text: value })}>speak</button>
    </div>
  );
}
export default Voice; 