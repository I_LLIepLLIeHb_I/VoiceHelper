import React from "react";
import Voice from "./VoicePage/Voice.jsx";
import "./App.css";

const App = () => {
  return (
    <div className="App"><Voice /> </div>
  );
};

export default App;